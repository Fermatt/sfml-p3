#ifndef EXPLOSION_H
#define EXPLOSION_H
#include "Object.h"

namespace Fermatt {
	class Explosion :
		public Object
	{
	private:
		float timer;
		const float frameTime;
		const float sizeImage;
		const float framesPerlist;
		int xAnim;
		int yAnim;
	public:
		Explosion();
		~Explosion();
		void update(sf::Time elapsed);
	};
}
#endif
