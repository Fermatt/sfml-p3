#ifndef BACKGROUND_H
#define BACKGROUND_H
#include "Object.h"

namespace Fermatt
{
	class BackGround :
		public Object
	{
	private:
		bool repeat;
	public:
		BackGround();
		BackGround(std::string s);
		BackGround(std::string s, bool _repeat);
		BackGround(int x, int y, std::string s);
		BackGround(int x, int y, std::string s, bool _repeat);
		~BackGround();
		virtual void update(sf::Time elapsed);
	};
}
#endif
