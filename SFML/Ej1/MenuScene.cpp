#include "MenuScene.h"
#include "GameScene.h"
#include "CreditsScene.h"
#include "Reg.h"

using namespace Fermatt;

MenuScene::MenuScene(sf::RenderWindow* window, Scene* _parentScene) :Scene(window), parentScene(_parentScene), sizeTitle(100), sizeRest(20), sizeEnter(40)
{
	titleFont.loadFromFile(Reg::assetPaths("title.ttf"));
	arialFont.loadFromFile(Reg::assetPaths("arialbd.ttf"));
	TitleText = new sf::Text("WRONG WAY!", titleFont, sizeTitle);
	TitleText->setPosition(Reg::winWidth / 2 - TitleText->getLocalBounds().width/2, Reg::winHeight/5);
	EnterText = new sf::Text("Press Enter to start", arialFont, sizeEnter);
	EnterText->setPosition(Reg::winWidth / 2 - EnterText->getLocalBounds().width / 2, Reg::winHeight - Reg::winHeight/4);
	ExitText = new sf::Text("Press Esc to exit", arialFont, sizeEnter);
	ExitText->setPosition(Reg::winWidth / 2 - ExitText->getLocalBounds().width / 2, Reg::winHeight - Reg::winHeight / 4 - EnterText->getLocalBounds().height*2);
	HighScoreText = new sf::Text("HighScore: " + std::to_string(parentScene->highScore), arialFont, sizeRest);
	HighScoreText->setPosition(0, 0);
	WeatherText = new sf::Text("Weather: " + parentScene->weather, arialFont, sizeRest);
	WeatherText->setPosition(Reg::winWidth - WeatherText->getLocalBounds().width, 0);
	sf::Color c = sf::Color::Black;
	TitleText->setFillColor(c);
	HighScoreText->setFillColor(c);
	WeatherText->setFillColor(c);
	titleSprite = new BackGround(Reg::assetPaths("backGround.png"));
	titleSprite->setPosition(0, 0);
	p = new Player(Reg::velPlayer, Reg::assetPaths("Player.png"));
	p->setPosition(Reg::winWidth / 2, Reg::winHeight / 2);
	music.openFromFile(Reg::assetPaths("title.ogg"));
	music.setLoop(true);
	music.play();
}


MenuScene::~MenuScene()
{
	delete titleSprite;
	delete p;
	delete TitleText;
	delete HighScoreText;
	delete WeatherText;
	delete EnterText;
	delete ExitText;
}

void MenuScene::update(sf::Time elapsed)
{
	window->draw(titleSprite->getSprite());
	window->draw(p->getSprite());
	window->draw(*TitleText);
	window->draw(*EnterText);
	window->draw(*ExitText);
	window->draw(*HighScoreText);
	window->draw(*WeatherText);
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return))
	{
		parentScene->changeScene(new GameScene(window, parentScene));
		//music.stop();
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
	{
		parentScene->changeScene(new CreditsScene(window, parentScene));
	}
}
