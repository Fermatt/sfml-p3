#include "Object.h"
#include <iostream>

using namespace Fermatt;

Object::Object(): margenDer(0), margenIzq(0)
{
}

Object::Object(std::string s): margenDer(0), margenIzq(0)
{
	if (!texture.loadFromFile(s))
	{
		std::cout << "Error, cant load image" << std::endl;
	}
	texture.setSmooth(true);
	sprite.setTexture(texture);
}

Object::Object(std::string s, sf::IntRect re) : margenDer(0), margenIzq(0)
{
	if (!texture.loadFromFile(s, re))
	{
		std::cout << "Error, cant load image" << std::endl;
	}
	texture.setSmooth(true);
	sprite.setTexture(texture);
}


Object::~Object()
{
}

void Object::setTexture(std::string s)
{
	if (!texture.loadFromFile(s))
	{
		std::cout << "Error, cant load image" << std::endl;
	}
	texture.setSmooth(true);
	sprite.setTexture(texture);
}

sf::Texture Object::getTexture() const
{
	return texture;
}

sf::Sprite Object::getSprite() const
{
	return sprite;
}

void Object::setPosition(int _x, int _y)
{
	sprite.setPosition(_x, _y);
}

void Object::setType(std::string s)
{
	type = s;
}

std::string Object::getType() const
{
	return type;
}

void Object::setMargen(int x, int y)
{
	margenIzq = x;
	margenDer = y;
}