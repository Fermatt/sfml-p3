#ifndef GAME_H
#define GAME_H

#include <SFML/Graphics.hpp>

namespace Fermatt {
	class Game
	{
	private:
		sf::RenderWindow* window;
	public:
		Game();
		~Game();
		int run();
		std::string getWeather();
	};
}
#endif