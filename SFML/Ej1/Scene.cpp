#include "Scene.h"

using namespace Fermatt;

Scene::Scene(sf::RenderWindow* window) :window(window)
{
}

Scene::Scene(sf::RenderWindow * window, std::string str, int hgs) : window(window), weather(str), highScore(hgs)
{
}

Scene::Scene(sf::RenderWindow* window, Scene* _s) :window(window), s(_s)
{
}

Scene::Scene(sf::RenderWindow * window, Scene * _s, std::string str, int hgs) : window(window), s(_s), weather(str), highScore(hgs)
{
}


Scene::~Scene()
{
	delete s;
	
}

void Scene::update(sf::Time elapsed)
{
	s->update(elapsed);
}

void Scene::changeScene(Scene* _s)
{
	Scene* aux = s;

	s = _s;
	delete aux;
}