#include "Explosion.h"
#include "Reg.h"

using namespace Fermatt;

Explosion::Explosion(): Object(Reg::assetPaths("ExplosionSprite.png")), xAnim(0), yAnim(0), timer(0), frameTime (0.05f), sizeImage(100), framesPerlist(9)
{
	setType("Explosion");
	sprite.setTextureRect(sf::IntRect(0, 0, sizeImage, sizeImage));
}


Explosion::~Explosion()
{
}

void Explosion::update(sf::Time elapsed)
{
	timer += elapsed.asSeconds();
	if (timer > frameTime)
	{
		timer = 0;
		xAnim++;
		if (xAnim >= framesPerlist)
		{
			xAnim = 0;
			yAnim++;
		}
		sprite.setTextureRect(sf::IntRect(sizeImage * xAnim, sizeImage * yAnim, sizeImage, sizeImage));
	}
}
