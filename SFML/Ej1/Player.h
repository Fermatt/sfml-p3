#ifndef PLAYER_H
#define PLAYER_H
#include "Object.h"

namespace Fermatt {
	class Player :
		public Object
	{
	private:
		const int velocity;
		int right;
		int up;
		bool dead;
		const int rotate;
	public:
		Player();
		Player(int v);
		Player(int v, std::string s);
		~Player();
		void moveUp();
		void moveDown();
		void moveLeft();
		void moveRight();
		void stopRight();
		void stopUp();
		void update(sf::Time elapsed);
		void setDead(bool d);
		void oilRotate();
		void Player::normalRotate();
		bool getDead() const;
	};
}
#endif