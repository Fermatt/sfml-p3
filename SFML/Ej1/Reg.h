#ifndef REG_H
#define REG_H
#ifdef NDEBUG
#define DIR "Release"
#else
#define DIR "Debug"
#endif

namespace Fermatt {
	class Reg
	{
	public:
		const static int winWidth = 800;
		const static int winHeight = 600;
		const static int velPlayer = 400;
		const static int velEnemy = 200;
		const static int velYEnemy = 400;
		const static int velXBG = 0;
		const static int velYBG = 150;
		const static std::string assetPaths(std::string s)
		{
			std::string i = DIR;
			return "../" + i + "/Assets/" + s;
		}
	};
}

#endif