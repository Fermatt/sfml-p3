#ifndef GAMESCENE_H
#define GAMESCENE_H
#include "Scene.h"
#include "Player.h"
#include "Enemy1.h"
#include "Explosion.h"
#include <SFML\Audio.hpp>

namespace Fermatt {
	class GameScene :
		public Scene
	{
	private:
		Scene* parentScene;
		Player* p;
		Explosion* e;
		std::vector<Object*> v;
		sf::Music music;
		sf::Music sfxEngine;
		sf::Music sfxExplosion;
		sf::Text* ScoreText;
		sf::Font arialFont;
		bool oilHit;
		bool justDead;
		int score;
		float timer;
		float timer2;
		float scoreTimer;
		float difference;
		const float maxTime;
		const float spawnMinus;
		const float maxTimeOil;
		const float maxTimeScore;
		const int sizeRest;
		const int deadTime;
		int margenIzq;
		int margenDer;
	public:
		GameScene(sf::RenderWindow* window, Scene* _parentScene);
		~GameScene();
		virtual void update(sf::Time elapsed);
		void movementPlayer(Player* p);
		void collisionPlayer(Player* p, Enemy1* e, bool kill);
	};
}
#endif