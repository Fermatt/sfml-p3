#include "BackGround.h"
#include "Reg.h"

using namespace Fermatt;

BackGround::BackGround(): repeat(false)
{
	setType("BackGround");
}

BackGround::BackGround(std::string s) : Object(s), repeat(false)
{
	setType("BackGround");
}

BackGround::BackGround(std::string s, bool _repeat) : Object(s), repeat(_repeat)
{
	setType("BackGround");
}

BackGround::BackGround(int x, int y, std::string s) : Object(s), repeat(false)
{
	setType("BackGround");
	sprite.setPosition(x, y);
}

BackGround::BackGround(int x, int y, std::string s, bool _repeat) : Object(s), repeat(_repeat)
{
	setType("BackGround");
	sprite.setPosition(x, y);
}

BackGround::~BackGround()
{
}

void BackGround::update(sf::Time elapsed)
{
	if (repeat)
	{
		sprite.move(Reg::velXBG*elapsed.asSeconds(), Reg::velYBG*elapsed.asSeconds());
		if (sprite.getPosition().y > Reg::winHeight)
		{
			sprite.setPosition(sprite.getPosition().x, -Reg::winHeight);
		}
	}
}
