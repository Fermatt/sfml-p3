#include "GameScene.h"
#include "MenuScene.h"
#include "Reg.h"
#include <iostream>
#include <fstream>

using namespace Fermatt;

GameScene::GameScene(sf::RenderWindow* window, Scene* _parentScene) :Scene(window), parentScene(_parentScene), score(0), maxTime(2), spawnMinus(0.01f), maxTimeOil(1), sizeRest(20), deadTime(4), difference(0), scoreTimer(0), timer(0), timer2(0), oilHit(false), justDead(false), maxTimeScore(0.5f)
{
	arialFont.loadFromFile(Reg::assetPaths("arialbd.ttf"));
	p = new Player(Reg::velPlayer, Reg::assetPaths("Player.png"));
	p->setPosition(Reg::winWidth / 2, Reg::winHeight / 2);
	v.push_back(new BackGround(Reg::assetPaths("leftSide.png"), true));
	v.push_back(new BackGround(0, -Reg::winHeight, Reg::assetPaths("leftSide.png"), true));
	margenIzq = v.back()->getSprite().getPosition().x + v.back()->getSprite().getLocalBounds().width;
	v.push_back(new BackGround(margenIzq, 0, Reg::assetPaths("centerSide.png"), true));
	v.push_back(new BackGround(margenIzq, -Reg::winHeight, Reg::assetPaths("centerSide.png"), true));
	margenDer = v.back()->getSprite().getPosition().x + v.back()->getSprite().getLocalBounds().width;
	v.push_back(new BackGround(margenDer, 0, Reg::assetPaths("rightSide.png"), true));
	v.push_back(new BackGround(margenDer, -Reg::winHeight, Reg::assetPaths("rightSide.png"), true));
	music.openFromFile(Reg::assetPaths("game.ogg"));
	music.setLoop(true);
	music.play();
	sfxEngine.openFromFile(Reg::assetPaths("motor.ogg"));
	sfxEngine.setLoop(true);
	sfxEngine.play();
	sfxExplosion.openFromFile(Reg::assetPaths("explosion.ogg"));
	oilHit = false;
	ScoreText = new sf::Text("Score: " + std::to_string(parentScene->highScore), arialFont, sizeRest);
	ScoreText->setPosition(0, 0);
	ScoreText->setFillColor(sf::Color::Black);
	p->setMargen(margenIzq, margenDer);
}


GameScene::~GameScene()
{
	delete p;
	for (int i = 0; i < v.size(); i++)
	{
		if(v[i] != NULL)
			delete v[i];
	}
	delete ScoreText;
	delete e;
}

void GameScene::update(sf::Time elapsed)
{
	scoreTimer += elapsed.asSeconds();
	if (scoreTimer > maxTimeScore)
	{
		score++;
		scoreTimer = 0;
	}
	ScoreText->setString("Score: " + std::to_string(score));
	if (!p->getDead())
	{
		if (!oilHit)
		{
			movementPlayer(p);
			p->update(elapsed);
		}
		else
		{
			p->oilRotate();
			timer2 += elapsed.asSeconds();
			if (timer2 > maxTimeOil)
			{
				oilHit = false;
				timer2 = 0;
				p->normalRotate();
			}
		}
		for (int i = 0; i < v.size(); i++)
		{
			if (v[i] != NULL)
			{
				v[i]->update(elapsed);
				if (v[i]->getType() == "Enemy")
					collisionPlayer(p, (Enemy1*)v[i], true);
				else if (v[i]->getType() == "Oil")
					collisionPlayer(p, (Enemy1*)v[i], false);
				window->draw(v[i]->getSprite());
				if (v[i]->getSprite().getPosition().y > Reg::winHeight)
				{
					delete v[i];
					v[i] = NULL;
				}
			}
		}
		window->draw(p->getSprite());
		timer += elapsed.asSeconds();
		if (timer > maxTime - difference)
		{
			switch (rand() % 3)
			{
			case 0:
				v.push_back(new Enemy1(rand() % (margenDer-margenIzq) + margenIzq, 0, Reg::velEnemy, Reg::assetPaths("car_blue_2.png")));
				break;
			case 1:
				v.push_back(new Enemy1(rand() % (margenDer - margenIzq) + margenIzq, 0, Reg::velEnemy, Reg::velYEnemy, Reg::assetPaths("car_red_1.png")));
				break;
			case 2:
				v.push_back(new Enemy1(rand() % (margenDer - margenIzq) + margenIzq, 0, Reg::velYBG, 0, Reg::assetPaths("oil.png"), true));
				break;
			}
			v.back()->setMargen(margenIzq, margenDer);
			difference += spawnMinus;
			timer = 0;
		}
		window->draw(*ScoreText);
	}
	else
	{
		if (justDead)
		{
			timer = 0;
			music.stop();
			justDead = false;
			sfxExplosion.play();
			e = new Explosion();
			e->setPosition(p->getSprite().getPosition().x - e->getSprite().getLocalBounds().width/2, p->getSprite().getPosition().y - e->getSprite().getLocalBounds().height / 2);
			p->setPosition(Reg::winWidth, Reg::winHeight);
		}
		for (int i = 0; i < v.size(); i++)
		{
			if (v[i] != NULL)
			{
				window->draw(v[i]->getSprite());
			}
		}
		e->update(elapsed);
		window->draw(e->getSprite());
		timer += elapsed.asSeconds();
		if (timer > deadTime)
		{
			if (parentScene->highScore < score)
			{
				parentScene->highScore = score;
				std::ofstream file(Reg::assetPaths("score.dat"));
				file << score << std::endl;
				file.close();
			}
			parentScene->changeScene(new MenuScene(window, parentScene));
		}
	}
}

void GameScene::movementPlayer(Player* p)
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
	{
		p->moveLeft();
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
	{
		p->moveRight();
	}
	else
	{
		p->stopRight();
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
	{
		p->moveUp();
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
	{
		p->moveDown();
	}
	else
	{
		p->stopUp();
	}
}

void GameScene::collisionPlayer(Player* p, Enemy1* e, bool kill)
{
	if (kill)
	{
		if (p->getSprite().getGlobalBounds().intersects(e->getSprite().getGlobalBounds()) && !p->getDead())
		{
			p->setDead(true);
			justDead = true;
		}
	}
	else
	{
		if (p->getSprite().getGlobalBounds().intersects(e->getSprite().getGlobalBounds()) && !p->getDead() && !oilHit)
		{
			oilHit = true;
		}
	}
}