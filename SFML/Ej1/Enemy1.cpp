#include "Enemy1.h"
#include "Reg.h"

using namespace Fermatt;

Enemy1::Enemy1(int x, int y, int v) : velY(v), velX(0)
{
	sprite.setPosition(x, y - sprite.getLocalBounds().height);
	setType("Enemy");
}

Enemy1::Enemy1(int x, int y, int v, std::string s) : Object(s), velY(v), velX(0)
{
	sprite.setPosition(x- sprite.getLocalBounds().width/2, y - sprite.getLocalBounds().height);
	setType("Enemy");
}

Enemy1::Enemy1(int x, int y, int v, int vy) : velY(v), velX(vy)
{
	sprite.setPosition(x, y - sprite.getLocalBounds().height);
	setType("Enemy");
}

Enemy1::Enemy1(int x, int y, int v, int vy, std::string s) : Object(s), velY(v), velX(vy)
{
	sprite.setPosition(x, y - sprite.getLocalBounds().height);
	setType("Enemy");
}

Enemy1::Enemy1(int x, int y, int v, int vy, std::string s, bool oil) : Object(s), velY(v), velX(vy)
{
	sprite.setPosition(x, y-sprite.getLocalBounds().height);
	if(oil)
		setType("Oil");
	else
		setType("Enemy");
}

Enemy1::~Enemy1()
{
}

void Enemy1::update(sf::Time elapsed)
{
	
	if (sprite.getPosition().x < margenIzq|| sprite.getPosition().x + sprite.getLocalBounds().width > margenDer)
	{
		velX *= -1;
		if (sprite.getPosition().x < margenIzq)
		{
			sprite.setPosition(margenIzq, sprite.getPosition().y);
		}
		else
		{
			sprite.setPosition(margenDer - sprite.getLocalBounds().width, sprite.getPosition().y);
		}
	}
	sprite.move(velX*elapsed.asSeconds(), velY*elapsed.asSeconds());
}
