#include "Player.h"
#include "Reg.h"

using namespace Fermatt;

Player::Player(): velocity(0), right(0), up(0), dead(false), rotate(10)
{
	setType("Player");
}

Player::Player(int v) :Object(), velocity(v), dead(false), rotate(10)
{
	setType("Player");
}

Player::Player(int v, std::string s) : Object(s), velocity(v), dead(false), rotate(10)
{
	setType("Player");
}

Player::~Player()
{
}

void Player::moveUp()
{
	if (0 < sprite.getPosition().y)
		up = -velocity;
	else
		up = 0;
}

void Player::moveDown()
{
	if (Reg::winHeight > sprite.getPosition().y)
		up = velocity;
	else
		up = 0;
}

void Player::moveLeft()
{
	if (margenIzq < sprite.getPosition().x)
		right = -velocity;
	else
		right = 0;
}

void Player::moveRight()
{
	if (margenDer > sprite.getPosition().x)
		right = velocity;
	else
		right = 0;
}

void Player::stopRight()
{
	right = 0;
}

void Player::stopUp()
{
	up = 0;
}

void Player::update(sf::Time elapsed)
{
	sprite.setOrigin(texture.getSize().x / 2, texture.getSize().y / 2);
	if (!dead)
	{
		sprite.move(right*elapsed.asSeconds(), up*elapsed.asSeconds());
	}
	else
	{
		sprite.setColor(sf::Color::White);
		dead = false;
	}
}

void Player::setDead(bool d)
{
	if (!dead && d)
	{
		dead = true;
		sprite.setColor(sf::Color::Transparent);
	}
}

void Player::oilRotate()
{
	sprite.rotate(rotate);
}

void Player::normalRotate()
{
	sprite.setRotation(0);
}

bool Player::getDead() const
{
	return dead;
}


