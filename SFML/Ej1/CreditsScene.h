#ifndef CREDITSSCENE_H
#define CREDITSSCENE_H
#include "Scene.h"
#include "BackGround.h"

namespace Fermatt {
	class CreditsScene :
		public Scene
	{
	private:
		Scene* parentScene;
		std::vector<sf::Text*> v;
		sf::Font arialFont;
		const int sizeRest;
		const int sizeTitle;
		const int difference;
		const int vel;
		void createText(std::string s, int size, int &dist);
	public:
		CreditsScene(sf::RenderWindow* window, Scene* _parentScene);
		~CreditsScene();
		virtual void update(sf::Time elapsed);
	};
}

#endif