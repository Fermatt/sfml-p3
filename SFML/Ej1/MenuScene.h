#ifndef MENUSCENE_H
#define MENUSCENE_H
#include "Scene.h"
#include "BackGround.h"
#include "Player.h"
#include <SFML\Audio.hpp>

namespace Fermatt {
	class MenuScene :
		public Scene
	{
	private:
		Scene* parentScene;
		BackGround* titleSprite;
		Player* p;
		sf::Text* TitleText;
		sf::Text* EnterText;
		sf::Text* ExitText;
		sf::Text* HighScoreText;
		sf::Text* WeatherText;
		sf::Font titleFont;
		sf::Font arialFont;
		sf::Music music;
		const int sizeTitle;
		const int sizeRest;
		const int sizeEnter;
	public:
		MenuScene(sf::RenderWindow* window, Scene* _parentScene);
		~MenuScene();
		virtual void update(sf::Time elapsed);
	};
}
#endif