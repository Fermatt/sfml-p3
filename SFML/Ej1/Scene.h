#ifndef SCENE_H
#define SCENE_H
#include <SFML\Graphics.hpp>

namespace Fermatt {
	class Scene
	{
	protected:
		sf::RenderWindow* window;
		Scene* s;
	public:
		Scene(sf::RenderWindow* window);
		Scene(sf::RenderWindow* window, std::string str, int hgs);
		Scene(sf::RenderWindow* window, Scene* _s);
		Scene(sf::RenderWindow* window, Scene* _s, std::string str, int hgs);
		virtual ~Scene();
		virtual void update(sf::Time elapsed);
		void changeScene(Scene* _s);
		const std::string weather;
		int highScore;
	};
}

#endif