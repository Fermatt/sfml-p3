#include "Game.h"
#include <windows.h>
#include <vld.h>

#ifdef NDEBUG
#define CONSOLE false
#else
#define CONSOLE true
#endif

using namespace Fermatt;

int main()
{
	if(!CONSOLE)
		ShowWindow(GetConsoleWindow(), SW_HIDE);
	Game g = Game();
	return g.run();
}