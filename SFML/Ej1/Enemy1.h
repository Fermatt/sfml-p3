#ifndef ENEMY1_H
#define ENEMY1_H
#include "Object.h"

namespace Fermatt {
	class Enemy1 :
		public Object
	{
	private:
		int velX;
		int velY;
	public:
		Enemy1(int x, int y, int v);
		Enemy1(int x, int y, int v, std::string s);
		Enemy1(int x, int y, int v, int vy);
		Enemy1(int x, int y, int v, int vy, std::string s);
		Enemy1(int x, int y, int v, int vy, std::string s, bool oil);
		~Enemy1();
		void update(sf::Time elapsed);
	};
}

#endif