#include "Game.h"
#include <fstream>

#include <SFML\Audio.hpp>
#include <SFML\Network.hpp>

#include <iostream>

#include <rapidjson\document.h>

#include "Reg.h"
#include "MenuScene.h"

#include <Windows.h>
#include <wininet.h>

using namespace rapidjson;
using namespace Fermatt;

Game::Game()
{
}


Game::~Game()
{
}

std::string Game::getWeather()
{
	bool i = true;
	if (system("ping www.google.com")) {
		i = false;
	}
	if (i)
	{

		sf::Http http("http://query.yahooapis.com/");
		std::string input;
		std::ifstream file(Reg::assetPaths("location.txt"));
		input = file.get();
		file.close();
		sf::Http::Request request;
		request.setMethod(sf::Http::Request::Get);
		request.setUri("v1/public/yql?q=select%20item.condition.text%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22" + input + "%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys");

		sf::Http::Response response = http.sendRequest(request);
		std::string s;
		Document document;
		document.Parse(response.getBody().c_str());
		if (document["query"]["count"].GetInt() == 1)
		{
			assert(document["query"]["results"]["channel"]["item"]["condition"]["text"].IsString());
			printf("Weather = %s\n", document["query"]["results"]["channel"]["item"]["condition"]["text"].GetString());
			s = document["query"]["results"]["channel"]["item"]["condition"]["text"].GetString();
		}
		else
		{
			s = "DOES NOT EXIST";
		}
		return s;
	}
	else
	{
		return "ERROR";
	}
}

int Game::run()
{
	srand(time(NULL));
	int score;
	std::ifstream file(Reg::assetPaths("score.dat"));
	if (file)
	{
		std::string input;
		while (file >> input);
		score = atoi(input.c_str());
	}
	else
	{
		std::ofstream highScore(Reg::assetPaths("score.dat"));
		highScore << "0" << std::endl;
		highScore.close();
		score = 0;
	}
	file.close();
	std::cout << score << std::endl;
	window = new sf::RenderWindow(sf::VideoMode(Reg::winWidth, Reg::winHeight), "SFML works!");
	std::string str = getWeather();
	sf::Clock clock;
	Scene* s = new Scene(window, str, score);
	s->changeScene(new MenuScene(window, s));
	while (window->isOpen())
	{
		sf::Time elapsed = clock.restart();
		sf::Event event;
		while (window->pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window->close();
		}
		window->clear();
		s->update(elapsed);
		window->display();
	}
	delete s;
	delete window;
	return 0;
}