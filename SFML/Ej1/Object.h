#ifndef OBJECT_H
#define OBJECT_H

#include <SFML/Graphics.hpp>

namespace Fermatt {
	class Object
	{
	protected:
		sf::Texture texture;
		sf::Sprite sprite;
		std::string type;
		int margenIzq;
		int margenDer;
	public:
		Object();
		Object(std::string s);
		Object(std::string s, sf::IntRect re);
		virtual ~Object();
		virtual void update(sf::Time elapsed) = 0;
		void setTexture(std::string s);
		sf::Texture getTexture() const;
		sf::Sprite getSprite() const;
		void setPosition(int _x, int _y);
		void setType(std::string s);
		std::string getType() const;
		void setMargen(int x, int y);
	};
}
#endif