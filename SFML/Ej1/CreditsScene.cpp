#include "CreditsScene.h"
#include "Reg.h"

using namespace Fermatt;

CreditsScene::CreditsScene(sf::RenderWindow* window, Scene* _parentScene) :Scene(window), parentScene(_parentScene), sizeRest(20), sizeTitle(30), difference(40), vel(-100)
{
	int dist = 0;
	arialFont.loadFromFile(Reg::assetPaths("arialbd.ttf"));
	createText("CREDITS", sizeTitle, dist);
	dist++;
	createText("Developed by Mat�as Fern�ndez", sizeRest, dist);
	dist+=2;
	createText("TOOLS", sizeTitle, dist);
	dist++;
	createText("Simple and Fast Multimedia Library (SFML)", sizeRest, dist);
	createText("Visual Studio 2015", sizeRest, dist);
	createText("Adobe Photoshop", sizeRest, dist);
	createText("RapidJson", sizeRest, dist);
	dist+=2;
	createText("HUGE THANKS TO THESE WEBSIDES", sizeTitle, dist);
	dist++;
	createText("Racing Pack (kenney.nl/assets/racing-pack)", sizeRest, dist);
	createText("Explosion (hasgraphics.com/tag/explosion/)", sizeRest, dist);
	createText("Car Engine SFX (opengameart.org/content/car-engine-loop-96khz-4s)", sizeRest, dist);
	createText("Explosion SFX (opengameart.org/content/2-high-quality-explosions)", sizeRest, dist);
	createText("Menu Music (opengameart.org/content/man-made-messiah)", sizeRest, dist);
	createText("Game Music (opengameart.org/content/vaerion-side-scrolling-shooter-music)", sizeRest, dist);
	createText("Breezed caps (www.fontspace.com/manfred-klein/breezed-caps)", sizeRest, dist);
	dist+=2;
	createText("THANK YOU FOR PLAYING!", sizeTitle, dist);
}


CreditsScene::~CreditsScene()
{
	for (int i = 0; i < v.size(); i++)
	{
		if (v[i] != NULL)
			delete v[i];
	}
}

void CreditsScene::update(sf::Time elapsed)
{
	for (int i = 0; i < v.size(); i++)
	{
		if (v[i] != NULL)
		{
			v[i]->move(0, vel*elapsed.asSeconds());
			window->draw(*v[i]);
		}
	}
	if (v.back()->getPosition().y + v.back()->getLocalBounds().height < 0)
	{
		window->close();
	}
}

void CreditsScene::createText(std::string s, int size, int &dist)
{
	v.push_back(new sf::Text(s, arialFont, size));
	v.back()->setPosition(Reg::winWidth / 2 - v.back()->getLocalBounds().width / 2, difference*dist + Reg::winHeight);
	v.back()->setFillColor(sf::Color::White);
	dist++;
}
